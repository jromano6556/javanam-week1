
public abstract class Account implements Detailable {

    // Properties
    private double balance;
    private String name;
    private static double interestRate;

    //Runs when class loads
    static{
        interestRate = .1;
    }

    // Constructors
    public Account(String name, double balance){
        this.name = name;
        this.balance = balance;
    }

    public Account(){
        this.name = "Jose";
        this.balance = 50;
    }

    // methods
    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double newInterestRate) {
		interestRate = newInterestRate;
	}

    public abstract void addInterest();

    public Double getBalance(){
        return this.balance;
    }

    public void setBalance(double newBal){
        this.balance = newBal;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String newName){
        this.name = newName;
    }

    public boolean withdraw(double amount){
        if(amount <= this.balance){
            this.balance -= amount;
            System.out.println("Withdrawl Completed");
            return true;
        }
        System.out.println("Withdrawl Failed");
        return false;
    }

    public boolean withdraw(){
        return this.withdraw(20);
    }

    @Override
    public String getDetails() {
        return "" + name + " " + balance;
    }

    @Override
    public String toString(){
        return this.getDetails();
    }
    
}