package com.conygre.simple;

public class HelloWorld {
    public static void main(String[] args) {
        String make = "BMW";
        String model = "530D";
        double engineSize = 3.0;
        byte gear = 2;
        short speed = 20;
        speed*=gear;
        int revs = speed*gear;

        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);
        System.out.println("The speed is gear*20 which is " + speed);
        System.out.println("The revs is gear*speed which is " + revs);

    }
}