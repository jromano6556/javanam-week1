package library_project.items;

import java.time.LocalDate;

public class LibraryItem {

    //Properties
    private String title;
    private String author;
    private Boolean isAvailable;
    private Boolean isBorrowable;
    private Type myType;
    private LocalDate returnDate;
    private LocalDate checkOutDate;
    private int intervalCheckoutPeriod;

    enum Type{
        BOOK,
        CD,
        DVD,
        PERIODICAL
    }

    enum Period{
        NA(0),
        DAY(1),
        WEEK(7),
        TWO_WEEK(14);

        private final int days;
        Period(int days) {this.days = days;}
        public int getValue() {return days;}
    }

    //Constructors

    public LibraryItem (String title, String author, Boolean isBorrowable, Type myType, int intervalCheckoutPeriod){
        this.title = title;
        this.author = author;
        this.isAvailable = true;
        this.isBorrowable = isBorrowable;
        this.myType = myType;
        this.intervalCheckoutPeriod = intervalCheckoutPeriod;
    }

    //Methods

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Boolean getIsAvailable() {
        return this.isAvailable;
    }

    public void setIsAvailable(Boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public Boolean getIsBorrowable() {
        return this.isBorrowable;
    }

    public void setIsBorrowable(Boolean isBorrowable) {
        this.isBorrowable = isBorrowable;
    }

    public Type getMyType() {
        return this.myType;
    }

    public void setMyType(Type myType) {
        this.myType = myType;
    }

    public LocalDate getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDate returnDate) {
        this.returnDate = returnDate;
    }

    public LocalDate getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(LocalDate checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public int getIntervalCheckoutPeriod() {
        return intervalCheckoutPeriod;
    }

    public void setIntervalCheckoutPeriod(int intervalCheckoutPeriod) {
        this.intervalCheckoutPeriod = intervalCheckoutPeriod;
    }

    public boolean equals(LibraryItem i){
        return (this.title.equals(i.getTitle()) && this.author.equals(i.getAuthor()) && this.myType == i.getMyType());
    }
    
}