package library_project.user;

import java.util.ArrayList;
import java.util.List;
import library_project.items.*;

public class User {
        // Properties
        private double balance;
        private String name;
        private static double lateFee;
        private List<LibraryItem> onLoan;
    
        //Runs when class loads
        static{
            lateFee = .05;
        }
    
        // Constructors
        public User(String name, double balance){
            this.name = name;
            this.balance = balance;
            this.onLoan = new ArrayList<LibraryItem>();
        }
    
        // methods
        public static double getlateFee() {
            return lateFee;
        }
    
        public static void setlateFee(double newlateFee) {
            lateFee = newlateFee;
        }
    
        public void addlateFee(int numDays){
            this.balance += numDays*lateFee;
        }
    
        public Double getBalance(){
            return this.balance;
        }
    
        public void setBalance(double newBal){
            this.balance = newBal;
        }
    
        public String getName(){
            return this.name;
        }
    
        public void setName(String newName){
            this.name = newName;
        }
        
        public boolean canCheckOut(){
            return (this.balance > 0);
        }

        public void checkOut(LibraryItem newItem){
            this.onLoan.add(newItem);
        }

        public boolean equals(User u){
            return this.name.equals(u.getName()) && this.balance == u.getBalance();
        }
    
}