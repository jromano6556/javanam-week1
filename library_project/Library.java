package library_project;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import library_project.items.*;
import library_project.user.*;

public class Library {
    //Properties
    //Notes don't use list use a hashmap or some map to store these items next time
    private List<LibraryItem> catalog;
    private List<User> registeredAccounts;

    //Constructors
    public Library(){
        catalog = new ArrayList<LibraryItem>();
        registeredAccounts = new ArrayList<User>();
    }

    //Methods

    public void addToCatalog(LibraryItem i){
        this.catalog.add(i);
    }

    public void addToAccounts(User u){
        this.registeredAccounts.add(u);
    }

    public LibraryItem find(LibraryItem i){
        for (LibraryItem item:this.catalog){
            if(item.equals(i)){
                return item;
            }
        }
        return null;
    }

    public Boolean findUser(User u){
        for(User registered:this.registeredAccounts){
            if(registered.equals(u)){
                return true;
            }
        }
        return false;
    }

    public LibraryItem findByTitle(String title){
        for (LibraryItem i:this.catalog){
            if(i.getTitle().equals(title)){
                return i;
            }
        }
        return null;
    }

    public LibraryItem findByCreator(String author){
        for (LibraryItem i:this.catalog){
            if(i.getAuthor().equals(author)){
                return i;
            }
        }
        return null;
    }

    public Boolean canCheckout(User u, LibraryItem i){
        LibraryItem item = find(i);
        return item!=null&& item.getIsAvailable() && item.getIsBorrowable() && findUser(u);
    }

    public void checkout(User u, LibraryItem i){
        if(canCheckout(u, i)){
            for (LibraryItem item:this.catalog){
                if(item.equals(i)){
                    item.setCheckOutDate(LocalDate.now());
                    for(User registered:this.registeredAccounts){
                        if(registered.equals(u)){
                            registered.checkOut(item);
                        }
                    }
                }
            }
        }
    }

    public void returnLibraryItem(User u, LibraryItem i){
        if(find(i)!=null){

        }
    }

    public List<LibraryItem> getCatalog() {
        return catalog;
    }

    public List<User> getRegisteredAccounts() {
        return registeredAccounts;
    }

}