package library_project;

import library_project.items.*;
import library_project.user.User;

public class LibraryTest {
    
    public static void main(String[] args) {
        Library myLibrary = new Library();
        User testUser = new User("Random Guy", 0);
        LibraryItem testBook = new Book("Random Book","Random Book Guy");
        LibraryItem testCD = new Cd("Random CD","Random CD Guy");
        LibraryItem testDVD = new Dvd("Random DVD","Random DVD Guy");
        LibraryItem testPeriodical = new Periodical("Random P","Random P Guy");
        myLibrary.addToCatalog(testBook);
        myLibrary.addToCatalog(testCD);
        myLibrary.addToCatalog(testDVD);
        myLibrary.addToCatalog(testPeriodical);
        myLibrary.addToAccounts(testUser);
        LibraryItem book = myLibrary.findByCreator("Random Book Guy");
        System.out.println(book.getTitle());
        System.out.println( myLibrary.canCheckout(testUser, book) ); 
        myLibrary.checkout(testUser, book);
    }
}