public class Elephant extends Animal {

    public Elephant(Gender gender) {
        super(gender, Diet.HERBIVORE, new Food[]{Food.GRASS, Food.FRUIT}, 80, 1);
    }
    
}