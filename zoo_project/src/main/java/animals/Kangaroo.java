public class Kangaroo extends Animal {

    public Kangaroo(Gender gender) {
        super(gender, Diet.HERBIVORE, new Food[]{Food.GRASS}, 80, 2);
    }
    
}