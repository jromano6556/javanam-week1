public class Meerkat extends Animal {

    public Meerkat(Gender gender) {
        super(gender, Diet.OMNIVORE, new Food[]{Food.GRASS, Food.EGGS, Food.BUGS}, 80, 8);
    }
    
}