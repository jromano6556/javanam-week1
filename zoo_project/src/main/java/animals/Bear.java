public class Bear extends Animal {

    public Bear(Gender gender) {
        super(gender, Diet.CARNIVORE, new Food[]{Food.MEAT, Food.FISH}, 80, 2);
    }
    
}