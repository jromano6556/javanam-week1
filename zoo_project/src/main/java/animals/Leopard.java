public class Leopard extends Animal {

    public Leopard(Gender gender) {
        super(gender, Diet.CARNIVORE, new Food[]{Food.MEAT}, 80, 4);
    }
    
}