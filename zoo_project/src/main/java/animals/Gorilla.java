public class Gorilla extends Animal {

    public Gorilla(Gender gender) {
        super(gender, Diet.HERBIVORE, new Food[]{Food.GRASS, Food.FRUIT}, 80, 1);
    }
    
}