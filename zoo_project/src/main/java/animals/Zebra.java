public class Zebra extends Animal {

    public Zebra(Gender gender) {
        super(gender, Diet.HERBIVORE, new Food[]{Food.GRASS}, 80, 1);
    }
    
}