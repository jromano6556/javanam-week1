import java.util.List;

public abstract class Animal {
    
    enum Gender { MALE, FEMALE; }
    enum Diet { HERBIVORE, CARNIVORE, OMNIVORE; }
    enum Food {GRASS, BUGS, EGGS, MEAT, FISH, FRUIT; }

    private Gender myGender;
    private Diet myDiet;
    private Food[] myFoods;
    private double temperature;
    private int litter;

    public Animal(Gender gender, Diet diet,  Food[] foods, double temp, int litter){
        this.myGender = gender;
        this.myDiet = diet;
        this.myFoods = foods;
        this.temperature = temp;
        this.litter = litter;
    }

	public Gender getMyGender() {
		return this.myGender;
	}

	public Diet getMyDiet() {
		return this.myDiet;
	}

	public Food[] getMyFoods() {
		return this.myFoods;
	}

	public double getTemperature() {
		return this.temperature;
	}

	public int getLitter() {
		return this.litter;
	}

}