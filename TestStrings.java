public class TestStrings {
    public static void main(String[] args) {
        String exampleString = "example.doc";
        System.out.println(exampleString);
        exampleString = exampleString.split("\\.")[0] + ".bak";
        System.out.println(exampleString);
        String test = "zabcd";
        if(!test.equals(exampleString)){
            System.out.println((test.compareTo(exampleString)>0?test:exampleString));
        }

        String test2 = "the quick brown fox swallowed down the lazy chicken";
        int fromIndex = 0;
        int count = 0;

        while(test2.indexOf("ow", fromIndex)!=-1){
            count++;
            fromIndex = test2.indexOf("ow", fromIndex)+1;
        }

        System.out.println(count);
        String test3 = "Live not on evil";
        System.out.println("Is string " + test3 + " a palindrome: "+isPalindrome(test3));

    }

    static boolean isPalindrome(String str){
        int start = 0;
        int end = str.length()-1;
        str = str.toLowerCase();
        while(start<end){
            if(str.charAt(start) != str.charAt(end)){
                return false;
            }
            start++;
            end--;
        }
        return true;
    }
}