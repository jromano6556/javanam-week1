import java.util.HashSet;
import java.util.Iterator;

public class TestAccount {
    public static void main(String[] args) {
        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};
        //Account[] accounts = new Account[5];
        HashSet<Account> accounts;
        accounts = new HashSet<Account>();
        Account.setInterestRate(.2);
        System.out.println("\nCurrent Accounts Test");
        for(int i=0;i<names.length;++i){
            accounts.add(new CurrentAccount(names[i],amounts[i]));
        }
        Iterator accs = accounts.iterator();
        while(accs.hasNext()){
            System.out.println(accs.next());
        }
        System.out.println("\nFor each test\n");
        for(Account a:accounts){
            System.out.println(a.getName() + " " + a.getBalance());
            a.addInterest();
            System.out.println(a.getDetails());
        }
        System.out.println("\nFor each method\n");
        accounts.forEach((acc)->{
            System.out.println(acc.getName() + " " + acc.getBalance());
            acc.addInterest();
            System.out.println(acc.getDetails());
        });
    }
}